﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace cloudser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudsolController : ControllerBase
    {
        // GET: api/<CloudsolController>
        [HttpGet]
        public IEnumerable<cloudmate> Get()
        {
            List<cloudmate> mates = new List<cloudmate>();
            mates.Add(new cloudmate { ID = "1", servicename = "cloudser", urllink = "//cloudsol/cloudsol" });

            mates.Add(new cloudmate { ID = "2", servicename = "cloudtek", urllink = "//cloudsol/cloudtek" });

            return mates;
        }

        // GET api/<CloudsolController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<CloudsolController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CloudsolController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CloudsolController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
